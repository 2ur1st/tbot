<?php

namespace App\Controller;

use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Request;
use Psr\Log\LoggerInterface;
use Longman\TelegramBot\Exception\TelegramException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class TbotController
 * @package App\Controller
 */
class TbotController extends Controller
{
    public function index()
    {
        return new JsonResponse([
            'Lucky number' => mt_rand(0, 100)
        ]);
    }

    public function setHook()
    {
        try {
            $hook = $this->generateUrl('hook', [], UrlGeneratorInterface::ABSOLUTE_URL);
            $telegram = new Telegram(getenv('BOT_API_KEY'), getenv('BOT_USERNAME'));
            $result = $telegram->setWebhook($hook);
            return new JsonResponse($result->getDescription());
        } catch (TelegramException $e) {
            return new JsonResponse($e->getMessage());
        } catch (\Exception $e) {
            return new JsonResponse($e->getMessage());
        }
    }

    public function hook(LoggerInterface $logger)
    {
        try {
            $commands_path = realpath(__DIR__ . '/../../Commands/');
            $telegram = new Telegram(getenv('BOT_API_KEY'), getenv('BOT_USERNAME'));
            $telegram->addCommandsPath($commands_path);
            $telegram->handle();
        } catch (TelegramException $e) {
            $logger->error($e->getMessage());
        } catch (\Exception $e) {
            $logger->error($e->getMessage());
        }
        return new JsonResponse();
    }
}